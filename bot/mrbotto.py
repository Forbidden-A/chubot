import datetime

import discord
from discord.ext import commands, tasks

import logging
import aiohttp
import asyncpg
import typing
import os
from importlib import resources

from bot.utils import blacklist


class MrB(commands.Bot):
    def __init__(self, config, **kwargs):
        kwargs.setdefault("command_prefix", self.get_prefix)
        kwargs.setdefault("case_insensitive", True)
        self.config = config
        self.logger = logging.getLogger(__name__)
        super().__init__(**kwargs)
        # Add the blacklist check to the bot
        self.add_check(blacklist.blacklist_check)

    # This is what discord.py invokes internally to log you in.
    # We connect to the database before we connect to discord at all.
    async def start(self, *args, **kwargs):
        self.dispatch("start")
        self.session = aiohttp.ClientSession(loop=self.loop)
        
        self.db = await asyncpg.create_pool(
            user=os.getenv("POSTGRES_USER", "postgres"),
            password=os.getenv("POSTGRES_PASSWORD", "postgres"),
            database=os.getenv("POSTGRES_DB", "postgres"),
            host=os.getenv("POSTGRES_HOST", "localhost"),
            port=int(os.getenv("POSTGRES_PORT", "5432")),
        )

        self.logger.info("Initialising database")
        schema = resources.read_text("bot", "schema.sql")
        await self.db.execute(schema)
        self.logger.info("Database initialisation complete")

        self.logger.info("db connected c:<")
        await super().start(*args, **kwargs)

    @staticmethod
    def create_time_string(num_seconds):
        m, s = divmod(num_seconds, 60)
        h, m = divmod(m, 60)
        d, h = divmod(h, 24)
        time_text = []
        if d:
            time_text.append(f"**{d}** days")
        if h:
            time_text.append(f"**{h}** hours")
        if m:
            time_text.append(f"**{m}** minutes")
        time_text.append(f"**{s}** seconds ago:")
        return ", ".join(time_text)

    @tasks.loop(seconds=5)
    async def poll_reminders(self, ctx):
        now = datetime.datetime.now(tz=datetime.timezone.utc)
        async with ctx.bot.db.acquire() as conn:
            completed_reminders = await conn.fetch(
                "SELECT reminder_id, start_time, message, user_id, channel_id FROM bot.reminders WHERE end_time <= $1",
                now,
            )

            for reminder in completed_reminders:
                seconds_ago = (now - reminder["start_time"]).seconds
                time_text = self.create_time_string(seconds_ago)
                channel = ctx.bot.get_channel(reminder["channel_id"])
                await channel.send(f"<@{reminder['user_id']}> {time_text} {reminder['message']}")
                await conn.execute(
                    "DELETE FROM bot.reminders WHERE reminder_id = $1",
                    reminder["reminder_id"],
                )

    async def logout(self):
        await self.db.close()
        await super().logout()

    async def on_connect(self):
        owner = (await self.application_info()).owner
        self.owner_id = owner.id
        self.logger.info("Looked up my owner. It is %s", owner)

    def get_cog(
        self, name: str
    ) -> typing.Optional[commands.Cog]:  # will override the existing behaviour
        """Gets a cog by its name, ignoring casing."""
        for cog_name, cog in self.cogs.items():
            if cog_name.casefold() == name.casefold():
                return cog

    async def get_prefix(self, message: discord.Message) -> typing.List[str]:
        """Gets a custom prefix for the given guild, or the default if in a DM."""
        default_prefix = "c!"
        prefixes = []

        if (
            message.guild and message.author.id == self.owner_id
        ):  # can easily set that property with application_info
            try:
                prefix = await self.db.fetchval(
                    "SELECT prefix FROM bot.prefixes WHERE guild_id = $1",
                    message.guild.id,
                )
                prefixes.append(prefix if prefix is not None else default_prefix)
                prefixes.extend([default_prefix, "broke ", "chu ", "Chu ", "11 "])
            except:
                prefixes += [default_prefix, f'{prefix} ', prefix, "broke ", "chu ", "Chu ", "11 "]
        elif not message.guild:
            prefixes.append(default_prefix)
        else:
            try:
                prefix = await self.db.fetchval(
                    "SELECT prefix FROM bot.prefixes WHERE guild_id = $1",
                    message.guild.id,
                )
                prefixes.append(prefix if prefix is not None else default_prefix)
            except:
                prefixes.append(default_prefix)

        return commands.when_mentioned_or(*prefixes)(self, message)
