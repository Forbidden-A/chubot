import aiohttp
import discord
import datetime
import random
import libneko

from discord.ext import commands


class Fun(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    def getAnswer(self, answerNumber):
        if answerNumber == 1:
            return "It is certain"
        elif answerNumber == 2:
            return "It is decidedly so"
        elif answerNumber == 3:
            return "Yes"
        elif answerNumber == 4:
            return "Reply hazy try again"
        elif answerNumber == 5:
            return "Ask again later"
        elif answerNumber == 6:
            return "Concentrate and ask again"
        elif answerNumber == 7:
            return "My reply is no"
        elif answerNumber == 8:
            return "Outlook not so good"
        elif answerNumber == 9:
            return "Very doubtful"

    @commands.cooldown(rate=5, per=1, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(name="8ball", aliases=["magic8ball"])
    async def eight_ball(self, ctx, *, input: str = None):
        """The magic 8ball decides your fate"""
        if input is None:
            return await ctx.send(
                f"{ctx.author.mention} This command needs input, smh."
            )
        r = random.randint(1, 9)
        fortune = self.getAnswer(r)

        em = discord.Embed(
            description=f"`Question:` {input}\n\n`The Magic 8Ball says:` {fortune}",
            timestamp=datetime.datetime.utcnow(),
            color=random.randint(0, 0xFFFFFF),
        )
        await ctx.send(embed=em)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()
    async def encode(self, ctx, *, text: str = None):
        """Encodes text into binary code"""
        if text is None:
            return await ctx.send(f"{ctx.author.mention} I can't encode nothing!")
        if len(text) > 30:
            return await ctx.send(
                f"{ctx.author.mention} Message can't be over 30 characters long. Did you know max characters for premium users is 100?"
            )
        async with aiohttp.request(
            "get", f"https://some-random-api.ml/binary?text={text}"
        ) as resp:
            data = await resp.json()
            code = data["binary"]
        embed = discord.Embed(
            description=f"```\n{code}```",
            color=random.randint(0, 0xFFFFFF),
            timestamp=datetime.datetime.utcnow(),
        )
        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()
    async def decode(self, ctx, *, code: str = None):
        """Decodes binary into text"""
        if code is None:
            return await ctx.send(f"{ctx.author.mention} I can't decode nothing!")
        async with aiohttp.request(
            "get", f"https://some-random-api.ml/binary?decode={code}"
        ) as resp:
            data = await resp.json()
            code = data["text"]
        embed = discord.Embed(
            description=f"```\n{code}```",
            color=random.randint(0, 0xFFFFFF),
            timestamp=datetime.datetime.utcnow(),
        )
        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=["raww", "r/aww"])  # r/aww generator
    async def aww(self, ctx, sort: str = "hot"):
        """Gets a post from r/aww

        Sorts by hot on default but you can sort by hot, top and new"""
        sort = sort.lower()
        if sort not in ("hot", "new", "top"):
            return await ctx.send(
                f"{ctx.author.mention} You can only sort by `hot`, `top` or `new`."
            )
        async with aiohttp.request(
            "get", f"https://www.reddit.com/r/aww/{sort}.json?sort=top"
        ) as r:
            # print(r.raise_for_status())
            res = await r.json()
            post = random.choice(res["data"]["children"])["data"]
            title = post["title"]
            url = post["url"]
            likes = post["ups"]
            comments = post["num_comments"]

            embed = discord.Embed(
                description=f"[{title}]({url})",
                timestamp=datetime.datetime.utcnow(),
                color=random.randint(0, 0xFFFFFF),
            )
            embed.set_footer(
                text=f"\N{THUMBS UP SIGN} {likes} | \N{SPEECH BALLOON} {comments}"
            )
            embed.set_image(url=post["url"])
            await ctx.send(
                content=f"`tip`, want to suggest a subreddit? use the `suggest` command!",
                embed=embed,
            )

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(
        aliases=[
            "animememe",
            "animemes",
            "ranimeme",
            "ranimememes",
            "r/animemes",
            "r/animeme",
        ]
    )  # r/Animemes generator
    async def animeme(self, ctx, sort: str = "hot"):
        """Gets a post from r/animemes

        Sorts by hot on default but you can sort by hot, top and new"""
        sort = sort.lower()
        if sort not in ("hot", "new", "top"):
            return await ctx.send(
                f"{ctx.author.mention} You can only sort by `hot`, `top` or `new`."
            )
        async with aiohttp.request(
            "get", f"https://www.reddit.com/r/animemes/{sort}.json?sort=top"
        ) as r:
            # print(r.raise_for_status())
            res = await r.json()
            post = random.choice(res["data"]["children"])["data"]
            title = post["title"]
            url = post["url"]
            likes = post["ups"]
            comments = post["num_comments"]

            embed = discord.Embed(
                description=f"[{title}]({url})",
                timestamp=datetime.datetime.utcnow(),
                color=random.randint(0, 0xFFFFFF),
            )
            embed.set_footer(
                text=f"\N{THUMBS UP SIGN} {likes} | \N{SPEECH BALLOON} {comments}"
            )
            embed.set_image(url=post["url"])
            await ctx.send(
                content=f"`tip`, want to suggest a subreddit? use the `suggest` command!",
                embed=embed,
            )

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(
        aliases=[
            "r/blessed",
            "r/blessedimages",
            "rblessed",
            "rblessedimages",
            "rblessedimage",
            "r/blessedimage",
            "blessed",
        ]
    )  # r/blessedimages generator
    async def blessedimages(self, ctx, sort: str = "hot"):
        """Gets a post from r/Blessed_images

        Sorts by hot on default but you can sort by hot, top and new"""
        sort = sort.lower()
        if sort not in ("hot", "new", "top"):
            return await ctx.send(
                f"{ctx.author.mention} You can only sort by `hot`, `top` or `new`."
            )
        async with aiohttp.request(
            "get", f"https://www.reddit.com/r/Blessed_images/{sort}.json?sort=top"
        ) as r:
            # print(r.raise_for_status())
            res = await r.json()
            post = random.choice(res["data"]["children"])["data"]
            title = post["title"]
            url = post["url"]
            likes = post["ups"]
            comments = post["num_comments"]

            embed = discord.Embed(
                description=f"[{title}]({url})",
                timestamp=datetime.datetime.utcnow(),
                color=random.randint(0, 0xFFFFFF),
            )
            embed.set_footer(
                text=f"\N{THUMBS UP SIGN} {likes} | \N{SPEECH BALLOON} {comments}"
            )
            embed.set_image(url=post["url"])
            await ctx.send(
                content=f"`tip`, want to suggest a subreddit? use the `suggest` command!",
                embed=embed,
            )

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(
        aliases=[
            "r/blursed",
            "r/blursedimages",
            "rblursed",
            "rblursedimages",
            "rblursedimage",
            "r/blursedimage",
            "blursed",
        ]
    )  # r/blursedimages generator
    async def blursedimages(self, ctx, sort: str = "hot"):
        """Gets a post from r/blursedimages

        Sorts by hot on default but you can sort by hot, top and new"""
        sort = sort.lower()
        if sort not in ("hot", "new", "top"):
            return await ctx.send(
                f"{ctx.author.mention} You can only sort by `hot`, `top` or `new`."
            )
        async with aiohttp.request(
            "get", f"https://www.reddit.com/r/blursedimages/{sort}.json?sort=top"
        ) as r:
            # print(r.raise_for_status())
            res = await r.json()
            post = random.choice(res["data"]["children"])["data"]
            title = post["title"]
            url = post["url"]
            likes = post["ups"]
            comments = post["num_comments"]

            embed = discord.Embed(
                description=f"[{title}]({url})",
                timestamp=datetime.datetime.utcnow(),
                color=random.randint(0, 0xFFFFFF),
            )
            embed.set_footer(
                text=f"\N{THUMBS UP SIGN} {likes} | \N{SPEECH BALLOON} {comments}"
            )
            embed.set_image(url=post["url"])
            await ctx.send(
                content=f"`tip`, want to suggest a subreddit? use the `suggest` command!",
                embed=embed,
            )

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # dank meme generator
    async def dankmeme(self, ctx, sort: str = "hot"):
        """Gets a post from r/dankmeme

        Sorts by hot on default but you can sort by hot, top and news"""
        sort = sort.lower()
        if sort not in ("hot", "new", "top"):
            return await ctx.send(
                f"{ctx.author.mention} You can only sort by `hot`, `top` or `new`."
            )
        async with aiohttp.request(
            "get", f"https://www.reddit.com/r/dankmemes/{sort}.json?sort=top"
        ) as r:
            # print(r.raise_for_status())
            res = await r.json()
            post = random.choice(res["data"]["children"])["data"]
            title = post["title"]
            url = post["url"]
            likes = post["ups"]
            comments = post["num_comments"]

            embed = discord.Embed(
                description=f"[{title}]({url})",
                timestamp=datetime.datetime.utcnow(),
                color=random.randint(0, 0xFFFFFF),
            )
            embed.set_footer(
                text=f"\N{THUMBS UP SIGN} {likes} | \N{SPEECH BALLOON} {comments}"
            )
            embed.set_image(url=post["url"])
            await ctx.send(
                content=f"`tip`, want to suggest a subreddit? use the `suggest` command!",
                embed=embed,
            )

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # meme generator
    async def meme(self, ctx, sort: str = "hot"):
        """Gets a post from r/memes

        Sorts by hot on default but you can sort by hot, top and new"""
        sort = sort.lower()
        if sort not in ("hot", "new", "top"):
            return await ctx.send(
                f"{ctx.author.mention} You can only sort by `hot`, `top` or `new`."
            )
        async with aiohttp.request(
            "get", f"https://www.reddit.com/r/memes/{sort}.json?sort=top"
        ) as r:
            # print(r.raise_for_status())
            res = await r.json()
            post = random.choice(res["data"]["children"])["data"]
            title = post["title"]
            url = post["url"]
            likes = post["ups"]
            comments = post["num_comments"]

            embed = discord.Embed(
                description=f"[{title}]({url})",
                timestamp=datetime.datetime.utcnow(),
                color=random.randint(0, 0xFFFFFF),
            )
            embed.set_footer(
                text=f"\N{THUMBS UP SIGN} {likes} | \N{SPEECH BALLOON} {comments}"
            )
            embed.set_image(url=post["url"])
            await ctx.send(
                content=f"`tip`, want to suggest a subreddit? use the `suggest` command!",
                embed=embed,
            )

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # Urban dictionary
    async def urban(self, ctx, *, word: str = None):
        """Searches something through the Urban Dictionary"""
        if word is None:
            await ctx.send(
                "What would you like to search? you can't search for nothing smh."
            )
            return
        try:
            async with aiohttp.request(
                "get", "http://api.urbandictionary.com/v0/define", params={"term": word}
            ) as resp:
                resp.raise_for_status()
                data = await resp.json()

            e2 = discord.Embed(
                title=f":mag:**Searched:** " + data["list"][0]["word"] + " :mag:",
                description=data["list"][0]["definition"],
                timestamp=datetime.datetime.utcnow(),
                color=random.randint(0, 0xFFFFFF),
            )
            e2.add_field(
                name="Example(s): ", value=data["list"][0]["example"], inline=False
            )
            e2.set_footer(
                text=f"\N{THUMBS UP SIGN} {data['list'][0]['thumbs_down']} | \N{THUMBS DOWN SIGN} {data['list'][0]['thumbs_up']}"
            )

            return await ctx.send(embed=e2)
        except Exception:
            return await ctx.send("Sorry we could find that word!")

    @commands.cooldown(rate=1, per=30, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(
        aliases=["song"],
        brief="Music lyrics",
        description="Search for the provided song's lyrics",
    )
    async def lyrics(self, ctx, *, query: str = None):
        """Shows you the lyrics to a song!"""
        async with aiohttp.request(
            "get", f"https://some-random-api.ml/lyrics?title={query}"
        ) as response:
            response = await response.json()
        if "lyrics" in response:

            @libneko.embed_generator(max_chars=700)
            def make_embed(paginator, page, index):
                em = discord.Embed(
                    title=f"{response['title']} - {response['author']}",
                    colour=random.randint(0, 0xFFFFFF),
                    description=f"```{page}```",
                    timestamp=datetime.datetime.utcnow(),
                    url=response["links"]["genius"],
                )
                em.set_footer(
                    text=f"Requested by {ctx.author.name}",
                    icon_url=ctx.author.avatar_url,
                )
                return em

            pag = libneko.EmbedNavigatorFactory(factory=make_embed, max_lines=30)
            for line in response["lyrics"].splitlines():
                pag.add_line(line)
            await pag.start(ctx=ctx, timeout=60.0)
            return
        await ctx.send(response["error"])

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()
    async def choose(
        self, ctx, *, choices: str = None
    ):  # Chooses something randomly from given choices
        """Chooses something randomly from your choice separated by a comma, this thing: ','"""
        choices = choices.split(",")

        if len(choices) <= 1 or choices is None:
            await ctx.send("Smh you need to actually tell me what to choose from.")

        else:
            embed = discord.Embed(
                title="I choose: " + random.choice(choices),
                description="\nYou're welcome",
                color=random.randint(0, 0xFFFFFF),
            )
            await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()
    async def echo(self, ctx, *, input: str = None):
        """Echos your input"""
        if input is None:
            return await ctx.send(
                f"{ctx.author.mention} command usage: `c!echo <input>`"
            )
        embed = discord.Embed(
            title="ECHO... Echo... echo...",
            description=input,
            color=random.randint(0, 0xFFFFFF)
        )
        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()
    async def reverse(self, ctx, *, input: str = None):
        """Reverses your input"""
        if input is None:
            return await ctx.send(
                f"{ctx.author.mention} command usage: `c!reverse <input>`"
            )
        embed = discord.Embed(
            title=random.choice(['Uno Reverse!', 'esreveR onU', 'Reverser Machine:', 'Uno Reverse!', 'esreveR onU', 'Reverser Machine:', 'Uno Reverse!', 'esreveR onU', 'Reverser Machine:', 'Uno Reverse!', 'esreveR onU', 'Reverser Machine:', 'Uno Reverse!', 'esreveR onU', 'Reverser Machine:', 'My Head Hurts']),
            description=input[::-1],
            color=random.randint(0, 0xFFFFFF)
        )
        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=["random"])  # random
    async def randomnumber(self, ctx, lower: int = None, upper: int = None):
        """chooses a number from your input"""
        if lower is None:
            await ctx.send(
                f"{ctx.author.mention} command usage: `randomnumber <input> <input>`"
            )
        elif upper is None:
            await ctx.send(
                f"{ctx.author.mention} command usage: `randomnumber <input> <input>`"
            )
        else:
            await ctx.send(random.randint(lower, upper))

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()
    async def roll(self, ctx, dice: int):  # rolls a dice
        """Roles a dice"""
        rolls = dice
        limit = dice
        if dice not in [6, 8, 12, 16, 20]:
            await ctx.send("dice can only be 6, 8, 12, 16 and 20!")
            return
        elif dice in [6, 8, 12, 16, 20]:
            result = str(random.randint(1, limit))
            await ctx.send(result)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # rates the user on their coolness
    async def coolrate(self, ctx, user: discord.Member = None):
        """Randomly rates the user on a scale of 1-100%"""
        embed = discord.Embed(title="Cool Rate", color=random.randint(0, 0xFFFFFF),)
        top = 100
        if user:
            embed.description = f"{user.mention} is {random.randint(0, top)}% cool!"
        else:
            embed.description = (
                f"{ctx.author.mention} is {random.randint(0, top)}% cool!"
            )

        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # rates the user on their gayness
    async def gayrate(self, ctx, user: discord.Member = None):
        """Randomly rates the user on a scale of 1-100%"""
        embed = discord.Embed(title="Gay Rate", color=random.randint(0, 0xFFFFFF),)
        top = 100
        if user:
            embed.description = f"{user.mention} is {random.randint(0, top)}% gay!"
        else:
            embed.description = (
                f"{ctx.author.mention} is {random.randint(0, top)}% gay!"
            )

        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # rates the user on their highness
    async def howhigh(self, ctx, user: discord.Member = None):
        """Randomly rates the user on a scale of 1-100%"""
        embed = discord.Embed(title="How High", color=random.randint(0, 0xFFFFFF),)

        top = 100
        high = random.randint(0, top)
        dec = random.randint(0, 1000)

        if high != 69:
            if user:
                embed.description = f"{user.mention} is {high}% high!"
            else:
                embed.description = f"{ctx.author.mention} is {high}% high!"

        if high == 69 and dec != 420:
            if user:
                embed.description = (
                    f"{user.mention} is {high}% high, SnoopDog would be proud!"
                )
            else:
                embed.description = (
                    f"{ctx.author.mention} is {high}% high, SnoopDog would be proud!"
                )

        if high == 69 and dec == 420:
            if user:
                embed.description = f"{user.mention} is {high}.{dec}% high, you know this is a 1 in 100000 chance right?"
            else:
                embed.description = f"{ctx.author.mention} is {high}.{dec}% high, you know this is a 1 in 100000 chance right?"

        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=["smartness"])
    async def iq(self, ctx, user: discord.Member = None):
        """Returns your 100% real IQ"""
        iq = random.randint(-51, 260)
        embed = discord.Embed(
            title="99.98% real IQ test", color=random.randint(0, 0xFFFFFF),
        )
        if user:
            embed.description = f"{user.mention}'s IQ: {iq}"
        else:
            embed.description = f"{ctx.author.mention}'s IQ: {iq}"
        return await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()
    async def pp(self, ctx, user: discord.Member = None):
        """Returns how big the users pp is"""
        pp = f"8{'=' * random.randint(0, 14)}D"
        embed = discord.Embed(
            title="100% accurate pp guesser", color=random.randint(0, 0xFFFFFF),
        )
        top = 100
        if user:
            embed.description = f"{user.mention}'s pp \n{pp}"
        else:
            embed.description = f"{ctx.author.mention}'s pp \n{pp}"

        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # rates the user on their weebness
    async def weebrate(self, ctx, user: discord.Member = None):
        """Randomly rates the user on a scale of 1-100%"""
        embed = discord.Embed(title="Weeb Rate", color=random.randint(0, 0xFFFFFF),)
        top = 100
        if user:
            embed.description = f"{user.mention} is {random.randint(0, top)}% weeb!"
        else:
            embed.description = (
                f"{ctx.author.mention} is {random.randint(0, top)}% weeb!"
            )

        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # what emote command
    async def bird(self, ctx):
        """Sends a cute bird image"""
        async with aiohttp.request(
            "get", "https://some-random-api.ml/img/birb"
        ) as resp:
            resp = await resp.json()
            link = resp["link"]
        embed = discord.Embed(
            title="Birdy!",
            timestamp=datetime.datetime.utcnow(),
            color=random.randint(0, 0xFFFFFF),
        )
        embed.set_image(url=link)
        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # what emote command
    async def cat(self, ctx):
        """Sends a cute cat image"""
        async with aiohttp.request("get", "https://some-random-api.ml/img/cat") as resp:
            resp = await resp.json()
            link = resp["link"]
        embed = discord.Embed(
            title="What a cute kitty :3",
            timestamp=datetime.datetime.utcnow(),
            color=random.randint(0, 0xFFFFFF),
        )
        embed.set_image(url=link)
        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # what emote command
    async def dog(self, ctx):
        """Sends a cute dog image"""
        async with aiohttp.request("get", "https://some-random-api.ml/img/dog") as resp:
            resp = await resp.json()
            link = resp["link"]
        embed = discord.Embed(
            title="A dog, how cute!",
            timestamp=datetime.datetime.utcnow(),
            color=random.randint(0, 0xFFFFFF),
        )
        embed.set_image(url=link)
        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # what emote command
    async def fox(self, ctx):
        """Sends a cute fox image"""
        async with aiohttp.request("get", "https://some-random-api.ml/img/fox") as resp:
            resp = await resp.json()
            link = resp["link"]
        embed = discord.Embed(
            title="A cute fox!",
            timestamp=datetime.datetime.utcnow(),
            color=random.randint(0, 0xFFFFFF),
        )
        embed.set_image(url=link)
        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # what emote command
    async def koala(self, ctx):
        """Sends a cute koala image"""
        async with aiohttp.request(
            "get", "https://some-random-api.ml/img/koala"
        ) as resp:
            resp = await resp.json()
            link = resp["link"]
        embed = discord.Embed(
            title="A koala bear!",
            timestamp=datetime.datetime.utcnow(),
            color=random.randint(0, 0xFFFFFF),
        )
        embed.set_image(url=link)
        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # what emote command
    async def panda(self, ctx):
        """Sends a cute panda image"""
        img = random.choice(["panda", "red_panda"])
        async with aiohttp.request(
            "get", f"https://some-random-api.ml/img/{img}"
        ) as resp:
            resp = await resp.json()
            link = resp["link"]
        embed = discord.Embed(
            title="A panda, how cute!",
            timestamp=datetime.datetime.utcnow(),
            color=random.randint(0, 0xFFFFFF),
        )
        embed.set_image(url=link)
        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Fun(bot))
