import io

import aiohttp
import urllib3
import datetime
import discord
import random
import re
import typing
import traceback
import json

from discord.ext import commands
from PIL import Image as PilImage, ImageDraw, ImageFont
from discord.ext.commands import converter

if not typing.TYPE_CHECKING:

    class UserFetchConverter(commands.Converter):
        async def convert(self, ctx, arg):
            try:
                return await commands.MemberConverter().convert(ctx, arg)
            except:
                return await ctx.bot.fetch_user(arg)


else:
    UserFetchConverter = discord.User


mcfont = ImageFont.truetype("sga.ttf", size=100)


def make(text, font=mcfont, fg=(0, 0, 0), bg=(255, 255, 255)):
    size = font.getsize(text, stroke_width=5)
    print(Image)
    img = PilImage.new("RGBA", (size[0] + 20, size[1] + 20), bg)
    d = ImageDraw.Draw(img)
    d.text((10, 10), text, fill=fg, font=font)
    return img


async def fetch(url: str, method: str = "GET", params=None):
    url = urllib3.util.parse_url(url)
    async with aiohttp.ClientSession() as session:
        if method == "POST":
            async with session.post(url=str(url), data=params) as response:
                return await response.json()
        else:
            async with session.get(str(url)) as response:
                print(response)
                print(await response.json())


with open("Config.json") as fp:
    imgpw = json.load(fp)


class Image(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    # 1) no user mentioned so lets check for an attachment in the command message
    async def find_attachment_in_message(self, m):
        if m.attachments:
            return m.attachments[0].url
        return None

    # 2) look for attachment in last 5 messages
    async def find_attachment_in_last_5_messages(self, channel):
        try:
            async for m in channel.history(limit=5):
                if not m.author.bot and m.attachments:
                    return await self.find_attachment_in_message(m)
        except discord.Forbidden:
            return None

    # 3) ok that failed lets just use the users avatar
    async def fetch_author_avatar(self, ctx):
        return str(ctx.author.avatar_url_as(format="png"))

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()
    async def comment(self, ctx, *, text: str = None):
        """Make a fake YouTube comment."""
        async with ctx.bot.session.get(
            "https://some-random-api.ml/canvas/youtube-comment",
            params={
                "avatar": str(ctx.author.avatar_url_as(format="png")),
                "username": ctx.author.display_name[:25],
                "comment": text,
            },
            raise_for_status=True,
        ) as resp:
            image = io.BytesIO(await resp.read())
            image.seek(0)
            return await ctx.send(file=discord.File(image, "youtube-comment.png"))

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=["e", "et", "mct"])
    async def enchant(self, ctx, *, text: str = None):
        """Translates english to minecraft enchantment table language"""
        if text is None:
            return await ctx.send(f"{ctx.author.mention} I can't enchant nothing!")
        alph = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,!? "
        if any(c not in alph for c in text):
            return await ctx.send(f"{ctx.author.mention} Sorry, this text contains invalid characters.")
        img = make(text, fg=(0xDC, 0xDD, 0xDF, 255), bg=(0, 0, 0, 0))
        img.save("tmp.png")
        await ctx.send(file=discord.File("tmp.png"))

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.command()
    @commands.guild_only()
    async def vs(
        self,
        ctx,
        str1: typing.Union[discord.Member, discord.User, str] = None,
        str2: typing.Union[discord.Member, discord.User, str] = None,
    ):
        """Randomly chooses something from your input"""
        try:
            if str1 is None or str2 is None:
                await ctx.send(
                    f"{ctx.author.mention} command usage: `c!vs <input> <input>`"
                )
                return
            url = "https://api.imgflip.com/caption_image"
            words = [
                str1,
                str2,
                str2,
                str1,
                str1,
                str2,
                str2,
                str1,
                str2,
                str2,
                str1,
                str1,
            ]
            choice = random.choice(words)
            second_choice = str1 if choice != str1 else str2
            params = {
                "username": imgpw["imgflip"]["username"],
                "password": imgpw["imgflip"]["password"],
                "template_id": 245800353,
                "text0": choice,
                "text1": second_choice,
            }
            response = await fetch(url=url, method="POST", params=params)
            image = response["data"]["url"]
            post = response["data"]["page_url"]
            embed = discord.Embed(
                title=f"{choice} is clearly better than {second_choice}",
                colour=random.randint(0, 0xFFFFFF),
                timestamp=datetime.datetime.utcnow(),
            )
            embed.add_field(
                name="image not showing?", value=f"[Click Here!]({post})", inline=False
            )
            embed.set_image(url=image)
            embed.set_footer(
                text=f"Requested by {ctx.author.display_name}",
                icon_url=ctx.author.avatar_url,
            )
            await ctx.send(embed=embed)
        except Exception as ex:
            user = (await self.bot.application_info()).owner
            await user.send(traceback.format_exc())

            await user.send(
                "".join(traceback.format_exception(type(ex), ex, ex.__traceback__))
            )

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.command()
    @commands.guild_only()
    async def gay(self, ctx, user: UserFetchConverter = None):
        """A rainbow overlay to an image"""
        data = None

        if user is not None:
            data = str(user.avatar_url_as(format="png"))

            # 2) and 3), as the history endpoint returns the command invocation too iirc.
        if data is None:
            data = await self.find_attachment_in_last_5_messages(ctx.channel)

        if data is None:
            data = await self.fetch_author_avatar(ctx)

        async with ctx.bot.session.get(
            "https://some-random-api.ml/canvas/gay",
            params={"avatar": data},
            raise_for_status=True,
        ) as resp:
            image = io.BytesIO(await resp.read())
            image.seek(0)
            return await ctx.send(file=discord.File(image, "pride.png"))

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.command()
    @commands.guild_only()
    async def fog(self, ctx, user: UserFetchConverter = None):
        """Overlays a fog texture over an image"""
        data = None

        if user is not None:
            data = str(user.avatar_url_as(format="png"))

            # 2) and 3), as the history endpoint returns the command invocation too iirc.
        if data is None:
            data = await self.find_attachment_in_last_5_messages(ctx.channel)

        if data is None:
            data = await self.fetch_author_avatar(ctx)

        async with ctx.bot.session.get(
            "https://some-random-api.ml/canvas/glass",
            params={"avatar": data},
            raise_for_status=True,
        ) as resp:
            image = io.BytesIO(await resp.read())
            image.seek(0)
            return await ctx.send(file=discord.File(image, "fog.png"))

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.command()
    @commands.guild_only()
    async def wasted(self, ctx, user: UserFetchConverter = None):
        """GTA wasted meme overlay"""
        data = None

        if user is not None:
            data = str(user.avatar_url_as(format="png"))

            # 2) and 3), as the history endpoint returns the command invocation too iirc.
        if data is None:
            data = await self.find_attachment_in_last_5_messages(ctx.channel)

        if data is None:
            data = await self.fetch_author_avatar(ctx)

        async with ctx.bot.session.get(
            "https://some-random-api.ml/canvas/wasted",
            params={"avatar": data},
            raise_for_status=True,
        ) as resp:
            image = io.BytesIO(await resp.read())
            image.seek(0)
            return await ctx.send(file=discord.File(image, "wasted.png"))

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.command()
    @commands.guild_only()
    async def triggered(self, ctx, user: UserFetchConverter = None):
        """Triggered meme overlay"""
        data = None

        if user is not None:
            data = str(user.avatar_url_as(format="png"))

            # 2) and 3), as the history endpoint returns the command invocation too iirc.
        if data is None:
            data = await self.find_attachment_in_last_5_messages(ctx.channel)

        if data is None:
            data = await self.fetch_author_avatar(ctx)

        async with ctx.bot.session.get(
            "https://some-random-api.ml/canvas/triggered",
            params={"avatar": data},
            raise_for_status=True,
        ) as resp:
            image = io.BytesIO(await resp.read())
            image.seek(0)
            return await ctx.send(file=discord.File(image, "triggered.gif"))

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.command()
    @commands.guild_only()
    async def greyscale(self, ctx, user: UserFetchConverter = None):
        """Grayscale overlay to an image"""
        data = None

        if user is not None:
            data = str(user.avatar_url_as(format="png"))

            # 2) and 3), as the history endpoint returns the command invocation too iirc.
        if data is None:
            data = await self.find_attachment_in_last_5_messages(ctx.channel)

        if data is None:
            data = await self.fetch_author_avatar(ctx)

        async with ctx.bot.session.get(
            "https://some-random-api.ml/canvas/greyscale",
            params={"avatar": data},
            raise_for_status=True,
        ) as resp:
            image = io.BytesIO(await resp.read())
            image.seek(0)
            return await ctx.send(file=discord.File(image, "greyscale.png"))

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.command()
    @commands.guild_only()
    async def invert(self, ctx, user: UserFetchConverter = None):
        """Invert's image colour"""
        data = None

        if user is not None:
            data = str(user.avatar_url_as(format="png"))

            # 2) and 3), as the history endpoint returns the command invocation too iirc.
        if data is None:
            data = await self.find_attachment_in_last_5_messages(ctx.channel)

        if data is None:
            data = await self.fetch_author_avatar(ctx)

        async with ctx.bot.session.get(
            "https://some-random-api.ml/canvas/invert",
            params={"avatar": data},
            raise_for_status=True,
        ) as resp:
            image = io.BytesIO(await resp.read())
            image.seek(0)
            return await ctx.send(file=discord.File(image, "invert.png"))

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.command()
    @commands.guild_only()
    async def invertgreyscale(self, ctx, user: UserFetchConverter = None):
        """Combination of invert and greyscale"""
        data = None

        if user is not None:
            data = str(user.avatar_url_as(format="png"))

            # 2) and 3), as the history endpoint returns the command invocation too iirc.
        if data is None:
            data = await self.find_attachment_in_last_5_messages(ctx.channel)

        if data is None:
            data = await self.fetch_author_avatar(ctx)

        async with ctx.bot.session.get(
            "https://some-random-api.ml/canvas/invertgreyscale",
            params={"avatar": data},
            raise_for_status=True,
        ) as resp:
            image = io.BytesIO(await resp.read())
            image.seek(0)
            return await ctx.send(file=discord.File(image, "invertgreyscale.png"))

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.command()
    @commands.guild_only()
    async def brightness(self, ctx, user: UserFetchConverter = None):
        """Brightens an image. Light mode on steroids..."""
        data = None

        if user is not None:
            data = str(user.avatar_url_as(format="png"))

            # 2) and 3), as the history endpoint returns the command invocation too iirc.
        if data is None:
            data = await self.find_attachment_in_last_5_messages(ctx.channel)

        if data is None:
            data = await self.fetch_author_avatar(ctx)

        async with ctx.bot.session.get(
            "https://some-random-api.ml/canvas/brightness",
            params={"avatar": data},
            raise_for_status=True,
        ) as resp:
            image = io.BytesIO(await resp.read())
            image.seek(0)
            return await ctx.send(file=discord.File(image, "lightlightmode.png"))

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.command()
    @commands.guild_only()
    async def sepia(self, ctx, user: UserFetchConverter = None):
        """Applies a sepia tint to an image"""
        data = None

        if user is not None:
            data = str(user.avatar_url_as(format="png"))

            # 2) and 3), as the history endpoint returns the command invocation too iirc.
        if data is None:
            data = await self.find_attachment_in_last_5_messages(ctx.channel)

        if data is None:
            data = await self.fetch_author_avatar(ctx)

        async with ctx.bot.session.get(
            "https://some-random-api.ml/canvas/sepia",
            params={"avatar": data},
            raise_for_status=True,
        ) as resp:
            image = io.BytesIO(await resp.read())
            image.seek(0)
            return await ctx.send(file=discord.File(image, "sepia.png"))

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.command(aliases=["color"])
    @commands.guild_only()
    async def colorize(
        self, ctx, colour: str = None, *, user: UserFetchConverter = None
    ):
        """Applies any color you want to an image.
        Only accepts HEX format."""
        if colour is None:
            return await ctx.send(f"{ctx.author.mention} Colour can't be nothing.")
        colour = colour.replace("#", "")
        data = None

        if user is not None:
            data = str(user.avatar_url_as(format="png"))

            # 2) and 3), as the history endpoint returns the command invocation too iirc.
        if data is None:
            data = await self.find_attachment_in_last_5_messages(ctx.channel)

        if data is None:
            data = await self.fetch_author_avatar(ctx)
        if match := re.match(r"^([0-9A-Fa-f]{6})$", colour):
            async with ctx.bot.session.get(
                "https://some-random-api.ml/canvas/color",
                params={"avatar": data, "color": "#" + colour},
                raise_for_status=True,
            ) as resp:
                image = io.BytesIO(await resp.read())
                image.seek(0)
                return await ctx.send(file=discord.File(image, "colour.png"))
        else:
            return await ctx.send(f"{ctx.author.mention} Invalid colour!")


def setup(bot):
    bot.add_cog(Image(bot))
