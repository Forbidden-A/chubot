import asyncio

from discord.ext import commands
import discord
import typing
import datetime
import random


if not typing.TYPE_CHECKING:

    class UserFetchConverter(commands.Converter):
        async def convert(self, ctx, arg):
            try:
                return await commands.MemberConverter().convert(ctx, arg)
            except:
                return await ctx.bot.fetch_user(arg)


else:
    UserFetchConverter = discord.User


class Moderation(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.cooldown(rate=1, per=3, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # ban command
    @commands.bot_has_permissions(ban_members=True)
    @commands.has_permissions(ban_members=True)
    async def ban(self, ctx, member: UserFetchConverter, *, reason: str = None):
        """Used for banning users"""
        if reason is None:
            reason = "No specified reason"
        if member == ctx.author:
            await ctx.send(f"{ctx.author.mention} You can't ban yourself!")
            return

        try:
            await ctx.guild.ban(member, reason=reason)
            await member.send(f"you were banned in {ctx.guild} for: `{reason}`")
            await ctx.send(f"{member} was banned for: `{reason}`")
        except:
            try:
                await ctx.guild.ban(member, reason=reason)
                await ctx.send(f"{member} was banned for: `{reason}`")
            except:
                await ctx.send(f"{ctx.author.mention} i cant ban that user!")

    @ban.error  # if they don't have perms
    async def on_error(self, ctx, error):
        if isinstance(error, commands.MissingPermissions):
            await ctx.send(f"{ctx.author.mention} You can't ban people!")
        elif isinstance(error, commands.BotMissingPermissions):
            await ctx.send(f"{ctx.author.mention} i can't do that!")
        elif isinstance(error, commands.MissingRequiredArgument):
            await ctx.send(f"{ctx.author.mention} you need to tell me who to ban!")
        else:
            raise error

    @commands.cooldown(rate=1, per=3, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()
    @commands.bot_has_permissions(ban_members=True)
    @commands.has_permissions(ban_members=True)
    async def bans(self, ctx):  # shows a list of bans
        """Shows a list of banned users"""
        count = await ctx.guild.bans()
        if not count or count is None:
            return await ctx.send(f"{ctx.author.mention} No one seems to banned.")
        bans = "\n".join(
            str(f"{ban.user.name: <32} | {ban.user.id}")
            for ban in await ctx.guild.bans()
        )
        embed1 = discord.Embed(
            title="Those in a better place",
            description=f"```{bans}```",
            color=random.randint(0, 0xFFFFFF),
        )
        await ctx.send(embed=embed1)

    @bans.error
    async def on_error(self, ctx, error):
        error = getattr(error, "original", error)
        if isinstance(error, commands.MissingPermissions):
            return await ctx.send(
                f"{ctx.author.mention} You don't have permissions to use this command!"
            )
        elif isinstance(error, commands.BotMissingPermissions):
            return await ctx.send(f"{ctx.author.mention} I don't have access to bans.")
        elif isinstance(error, discord.Forbidden):
            return await ctx.send(f"{ctx.author.mention} I don't have access to bans.")
        else:
            user = (await self.bot.application_info()).owner
            error = getattr(error, "original", error)
            return await user.send(error)

    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    @commands.command()
    async def unban(self, ctx, user_id: int):
        """Unbans a user using their id
        Use the bans command to find a banned users id"""
        user = discord.Object(user_id)

        try:
            # undocumented, see https://github.com/Rapptz/discord.py/issues/5080#issue-647565801
            ban_entry = await ctx.guild.fetch_ban(user)
            await ctx.guild.unban(user)
            await ctx.send(f"Unbanned user {ban_entry.user}")
        except discord.NotFound:
            await ctx.send(
                f"{ctx.author.mention} That user doesnt exist, or isn't banned."
            )

    @commands.cooldown(rate=1, per=3, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # kick command
    @commands.bot_has_permissions(kick_members=True)
    @commands.has_permissions(kick_members=True)
    async def kick(self, ctx, member: discord.User, *, reason: str = None):
        """Used for kicking users"""
        if reason is None:
            reason = "No specified reason"
        if member == ctx.author:
            return await ctx.send(f"{ctx.author.mention} You can't kick yourself!")

        try:
            await ctx.guild.kick(member, reason=reason)
            await member.send(f"you were kicked in {ctx.guild} for: `{reason}`")
            await ctx.send(f"{member} was kicked for: `{reason}`"),
        except:
            try:
                await ctx.guild.kick(member, reason=reason)
                return await ctx.send(f"{member} was kicked for: `{reason}`")
            except:
                return await ctx.send(f"{ctx.author.mention} i cant kick that user!")

    @kick.error  # if they don't have perms to kick
    async def on_error(self, ctx, error):
        if isinstance(error, commands.MissingPermissions):
            await ctx.send(f"{ctx.author.mention} You can't kick people!")
        elif isinstance(error, commands.BotMissingPermissions):
            await ctx.send(f"{ctx.author.mention} i can't kick people!")
        elif isinstance(error, commands.MissingRequiredArgument):
            await ctx.send(f"{ctx.author.mention} you need to tell me who to kick!")
        else:
            raise error

    @commands.has_permissions(manage_messages=True)
    @commands.guild_only()
    @commands.command()
    async def purge(self, ctx, amount: int = None, user: discord.Member = None):
        """Used for purging messages"""
        oldest = datetime.datetime.utcnow() - datetime.timedelta(days=14)
        if amount is None:
            return await ctx.send(f"{ctx.author.mention} I can't purge 0 messages!")
        if amount > 100:
            return await ctx.send(
                f"{ctx.author.mention} I can't purge more the 100 messages!"
            )
        if not user:
            await ctx.message.delete()
            messages = ctx.channel.history(after=oldest, limit=amount)
            messages_removed = await ctx.channel.purge(
                limit=len(await messages.flatten())
            )
            if len(messages_removed) == 0:
                error_embed = discord.Embed(
                    description=":x: **Error:** i can't purge messages over two weeks old!",
                    timestamp=datetime.datetime.utcnow(),
                    color=random.randint(0, 0xFFFFFF),
                )
                return await ctx.send(embed=error_embed, delete_after=3)
            else:
                embed2 = discord.Embed(
                    description=f"I cleared {len(messages_removed)} messages, happy now?",
                    timestamp=datetime.datetime.utcnow(),
                    color=random.randint(0, 0xFFFFFF),
                )
                embed2.set_footer(
                    text=f"Responsible moderator: {ctx.author.display_name}",
                    icon_url=ctx.author.avatar_url,
                )
                await ctx.send(embed=embed2, delete_after=3)
        else:
            await ctx.message.delete()

            def purge_type(m):
                return m.author == user

            messages = ctx.channel.history(after=oldest, limit=amount)
            messages_removed = await ctx.channel.purge(
                limit=len(await messages.flatten()), check=purge_type
            )
            if len(messages_removed) == 0:
                error_embed = discord.Embed(
                    description=":x: **Error:** i can't purge messages over two weeks old!",
                    timestamp=datetime.datetime.utcnow(),
                    color=random.randint(0, 0xFFFFFF),
                )
                return await ctx.send(embed=error_embed, delete_after=3)

            else:
                embed2 = discord.Embed(
                    description=f"I cleared {len(messages_removed)} messages from {user.mention}, happy now?",
                    timestamp=datetime.datetime.utcnow(),
                    color=random.randint(0, 0xFFFFFF),
                )
                embed2.set_footer(
                    text=f"Responsible moderator: {ctx.author.display_name}",
                    icon_url=ctx.author.avatar_url,
                )
                await ctx.send(embed=embed2, delete_after=3)

    @purge.error  # if they don't have perms
    async def on_error(self, ctx, error):
        if isinstance(error, commands.MissingPermissions):
            await ctx.send(f"{ctx.author.mention} You can't purge messages!")
        elif isinstance(error, commands.BotMissingPermissions):
            await ctx.send(f"{ctx.author.mention} i can't purge messages!")
        else:
            user = (await self.bot.application_info()).owner
            await user.send(f"```py\n{error}```")

    @commands.cooldown(rate=1, per=120, type=commands.BucketType.user)
    @commands.has_permissions(manage_guild=True)
    @commands.guild_only()
    @commands.command()
    async def setprefix(self, ctx, prefix: str = None):
        """Changes my prefix!"""
        if prefix is None:
            ctx.command.reset_cooldown(ctx)
            return await ctx.send(f"{ctx.author.mention} Prefix can't be nothing!")
        elif len(prefix) > 5:
            ctx.command.reset_cooldown(ctx)
            return await ctx.send(
                f"{ctx.author.mention} Prefix can't be more then 5 characters long!"
            )
        elif " " in prefix:
            ctx.command.reset_cooldown(ctx)
            return await ctx.send(f"{ctx.author.mention} Prefix can't include spaces!")
        elif ctx.prefix == prefix:
            ctx.command.reset_cooldown(ctx)
            return await ctx.send(
                f"{ctx.author.mention} Prefix can't be the same as the old one!"
            )
        else:
            await ctx.send(
                f"{ctx.author.mention} Are you sure you want to change the current prefix to `{prefix}`? \n\nSay `confirm` if correct or say `cancel` to cancel!"
            )

        def check(message):
            """Checks if the person who started the command is the same as the one confirming it"""
            return (
                message.author.id == ctx.message.author.id
                and message.channel.id == ctx.message.channel.id
            )

        try:
            tries = 0
            while tries != 3:
                msg = await self.bot.wait_for("message", timeout=60.0, check=check)
                if msg.content == "cancel":
                    ctx.command.reset_cooldown(ctx)
                    return await ctx.send("Cancelled!")
                elif msg.content == "confirm":
                    ctx.command.reset_cooldown(ctx)
                    await self.bot.db.execute(
                        "INSERT INTO bot.prefixes (guild_id, prefix) VALUES($1, $2) ON CONFLICT (guild_id) DO UPDATE SET prefix = $2;",
                        ctx.guild.id,
                        prefix,
                    )
                    return await ctx.send(
                        f"{ctx.author.mention} Your prefix has changed to `{prefix}`"
                    )
                else:
                    tries += 1
                    if tries != 3:
                        await ctx.send(
                            f"{ctx.author.mention} That's not a valid option, please try again!"
                        )
                    else:
                        return await ctx.send(
                            f"{ctx.author.mention} To many failed attempts, please try again later."
                        )

        except asyncio.TimeoutError:
            return await ctx.send(
                f"{ctx.author.mention} You took to long, prefix change has been canceled."
            )

    @setprefix.error  # if they don't have perms
    async def on_error(self, ctx, error):
        if isinstance(error, commands.MissingPermissions):
            await ctx.send(
                f"{ctx.author.mention} You don't have permissions to change my prefix!"
            )
        else:
            raise

    @commands.has_permissions(kick_members=True)
    @commands.guild_only()
    @commands.command()
    async def leave(self, ctx):
        """Makes me leave the server"""
        await ctx.send("Bye, thanks for nothing.")
        return await ctx.guild.leave()

    @leave.error
    async def on_error(self, ctx, error):
        if isinstance(error, commands.MissingPermissions):
            return await ctx.send(f"{ctx.author.mention} This command requires the `kick members` permission.")


def setup(bot):
    bot.add_cog(Moderation(bot))
