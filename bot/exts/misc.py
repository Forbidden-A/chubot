import asyncio

from discord.ext import commands
import datetime
import discord
import random


class Misc(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.cooldown(rate=1, per=20, type=commands.BucketType.user)
    @commands.command(aliases=["info", "bot", "contribute", "support"])  # Help command
    async def botinfo(self, ctx):
        """Displays a list of awesome people who deserve credit."""

        e = discord.Embed(
            timestamp=datetime.datetime.utcnow(), color=random.randint(0, 0xFFFFFF)
        )
        e.add_field(
            name="Developer info:",
            value=f"I was made by Chuboñyo™#6969, with tons of help from others! use {ctx.prefix}credit for a list of core contibutors!",
            inline=False,
        )
        e.add_field(
            name="support:",
            value="For support [click here](https://discord.gg/DgPfrHN)!\nWant to contribute [click here](https://gitlab.com/Chubonyo/chubot)!",
            inline=False,
        )

        await ctx.send(embed=e)

    @commands.cooldown(rate=1, per=20, type=commands.BucketType.user)
    @commands.command(aliases=["credits"])  # Help command
    async def credit(self, ctx):
        """Displays a list of awesome people who deserve credit."""
        e = discord.Embed(
            title="A list of awesome people!",
            timestamp=datetime.datetime.utcnow(),
            color=random.randint(0, 0xFFFFFF),
        )
        e.add_field(
            name="nekoka#8788",
            value="For being the biggest help out of everyone who has contributed, nekoka has taught me so much about coding and has been there for me when i needed it, i couldn't have gotten this far without them!",
            inline=False,
        )
        e.add_field(
            name="davfsa#7026",
            value="For helping fix a lot of bugs and teaching me a lot that i still use to this day",
            inline=False,
        )
        e.add_field(
            name="Forbidden#0001",
            value="For making the first version of the server info command and teaching me how to use API's",
            inline=False,
        )
        e.add_field(
            name="CorpNewt#4290",
            value="For being one of the big reasons and inspirations for making a bot in the first place!",
            inline=False,
        )
        e.add_field(
            name="thomm.o#8637",
            value="For helping with a lot of the annoying database stuff and getting me out of bad coding habits.",
            inline=False,
        )
        e.add_field(name="M.A#4999", value="ok", inline=False)
        e.add_field(
            name="Special thanks to:",
            value="[Discord Coding Academy](https://discord.gg/RDKfMcC), their staff team and bot developers for teaching me a lot, they were a huge help",
            inline=False,
        )
        e.set_footer(text="Anyone who made it to this list is awesome!")

        await ctx.send(embed=e)

    @commands.guild_only()
    @commands.cooldown(rate=2, per=3600, type=commands.BucketType.user)
    @commands.command()  # suggestion command
    async def suggest(self, ctx, *, suggestion: str = None):
        """Make a suggestion, sent directly to the owner.

        Do NOT abuse this, doing so will get you blacklisted."""

        if suggestion is None:
            await ctx.send(f"{ctx.author.mention} You can't suggest nothing.")
        else:
            user = (await self.bot.application_info()).owner
            embed = discord.Embed(
                title=f"User: {ctx.message.author} ID: {ctx.author.id} suggested:",
                description=f"{suggestion}"[:500],
                timestamp=datetime.datetime.utcnow(),
                color=random.randint(0, 0xFFFFFF),
            )
            await user.send(embed=embed)
            embed = discord.Embed(
                description=f"Your suggestion has been sent {ctx.author.mention}",
                timestamp=datetime.datetime.utcnow(),
                color=random.randint(0, 0xFFFFFF),
            )
            await ctx.send(embed=embed)
            await ctx.message.delete()


def setup(bot):
    bot.add_cog(Misc(bot))
