import asyncio
import aiohttp
import time
import typing
import traceback
import discord
import datetime
import logging
import random
import unicodedata
import dateparser

from decimal import Decimal
from dateparser import search
from discord.ext import commands
from discord.ext import tasks
from collections import Counter
from pytz import timezone


async def is_valid_image(url):
    async with aiohttp.request("head", url) as resp:
        if 200 <= resp.status < 300:
            return resp.content_type in ["image/png", "image/gif"]
    return False


if not typing.TYPE_CHECKING:

    class UserFetchConverter(commands.Converter):
        async def convert(self, ctx, arg):
            try:
                return await commands.MemberConverter().convert(ctx, arg)
            except:
                return await ctx.bot.fetch_user(arg)


else:
    UserFetchConverter = discord.User


class Utility(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.logger = logging.getLogger(__name__)
        self.format = "%Y/%m/%d %H:%M"
        self.timezone = timezone("UTC")

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=["unicode", "characterinfo"])
    async def charinfo(self, ctx, *, characters: str):
        """Shows you information about a number of characters.
        Only up to 25 characters at a time.
        """
        try:

            def to_string(c):
                digit = f"{ord(c):x}"
                name = unicodedata.name(c, "Name not found.")
                return f"[`\\U{digit:>08}`](http://www.fileformat.info/info/unicode/char/{digit}): {name} | {c} | \N{EM DASH}"

            msg = "\n".join(map(to_string, characters))
            if len(msg) > 2000:
                return await ctx.send("Output too long to display.")
            embed = discord.Embed(
                description=msg,
                color=random.randint(0, 0xFFFFFF),
                timestamp=datetime.datetime.utcnow(),
            )
            await ctx.send(embed=embed)
        except:
            return await ctx.send(f"{ctx.author.mention} This command needs input")

    @commands.cooldown(rate=1, per=20, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=["server", "guild", "serverinfo", "here"])
    async def guildinfo(self, ctx: commands.Context):
        """
        returns information about the current guild
        """
        embed = discord.Embed(
            title=f"**Information about __{ctx.guild.name}__:**",
            color=random.randint(0, 0xFFFFFF),
            timestamp=ctx.guild.created_at,
        )
        embed.set_footer(
            text=f"Guild owner: {ctx.guild.owner.name}#{ctx.guild.owner.discriminator} | Created at",
            icon_url=ctx.guild.owner.avatar_url,
        )
        boosts_message = f"Boost Level: {ctx.guild.premium_tier}\nBoosts: {ctx.guild.premium_subscription_count}"
        if ctx.guild.premium_subscription_count > 0:
            embed.add_field(name="**Boosts**", value=boosts_message, inline=True)
        channels_message = f"<:text_channel:713183384825888860> {str(len(ctx.guild.text_channels))} <:voice_channel:713183407747629208> {str(len(ctx.guild.voice_channels))}"
        member_by_status = Counter(str(m.status) for m in ctx.guild.members)
        members = (
            f'<:online:713182866703384634> {member_by_status["online"]}\n'
            f'<:idle:713182899024691280> {member_by_status["idle"]}\n'
            f'<:dnd:713182927202156614> {member_by_status["dnd"]}\n'
            f'<:offline:713183001596657724> {member_by_status["offline"]}\n'
            f"Total: {ctx.guild.member_count}"
        )
        embed.add_field(
            name="**General info**",
            value=f"**Guild region**: {ctx.guild.region}\n**Guild ID**: {ctx.guild.id}\n**Emotes**: {len(ctx.guild.emojis)}\n**Channels**: {channels_message}",
            inline=True,
        )
        embed.add_field(name="**Members**", value=members, inline=True)
        roles = list(ctx.guild.roles)
        roles.reverse()
        rolestr = ", ".join(
            [role.mention for role in roles if "@everyone" not in role.name]
        )
        if len(ctx.guild.roles) > 31:
            embed.add_field(name=f"**Roles** ({len(roles) - 1})", inline=True)
        else:
            embed.add_field(
                name=f"**Roles** ({len(roles) - 1})", value=rolestr, inline=True
            )
        embed.set_thumbnail(url=ctx.guild.icon_url)
        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=["whois", "who", "user"])
    async def userinfo(self, ctx, member: discord.Member = None):
        """returns information about the provided user or you if no user provided"""
        member = member or ctx.author
        icon_url = member.avatar_url
        status = "Unknown"
        status = (
            "<:online:713182866703384634>"
            if member.status == discord.Status.online
            else status
        )
        status = (
            "<:dnd:713182927202156614>"
            if member.status == discord.Status.dnd
            or member.status == discord.Status.do_not_disturb
            else status
        )
        status = (
            "<:idle:713182899024691280>"
            if member.status == discord.Status.idle
            else status
        )
        status = (
            "<:offline:713183001596657724>"
            if member.status == discord.Status.offline
            else status
        )
        embed = discord.Embed(
            title=f"**Information about {status} {member.display_name}:**",
            colour=random.randint(0, 0xFFFFFF),
            timestamp=datetime.datetime.utcnow(),
        )
        embed.set_footer(
            text=f"Full name: {member.name}#{member.discriminator} | Created at {self.timezone.localize(member.created_at).astimezone(self.timezone).strftime(self.format)}",
            icon_url=icon_url,
        )
        embed.set_author(name=" ", icon_url=member.avatar_url, url=member.avatar_url)
        embed.add_field(
            name=f'**Joined "{ctx.guild.name}" at**',
            value=self.timezone.localize(member.joined_at)
            .astimezone(self.timezone)
            .strftime(self.format),
            inline=True,
        )
        embed.add_field(name="**ID**", value=member.id, inline=True)
        boosting_value = (
            f"Boosting since: {self.timezone.localize(member.premium_since).astimezone(self.timezone).strftime(self.format)}"
            if member.premium_since is not None
            else "Not Boosting"
        )
        if member.premium_since is not None:
            embed.add_field(name="**Boosting status**", value=boosting_value)
        roles = list(member.roles)
        roles.reverse()
        rolestr = " ".join(
            [role.mention for role in roles if "@everyone" not in role.name]
        )
        if len(member.roles) >= 12:
            rolestr = f"{len(roles) - 1}"
            embed.add_field(name="**Roles**", value=f"{rolestr}")
        else:
            embed.add_field(name=f"**Roles ({len(roles) - 1})**", value=f"{rolestr}")
        embed.set_thumbnail(url=str(member.avatar_url).replace("1024", "2048"))
        await ctx.send(embed=embed)

    @userinfo.error
    async def on_error(self, ctx, error):
        if not isinstance(error, commands.CommandOnCooldown):
            return await ctx.send(
                f"{ctx.author.mention} I'm sorry i couldn't find that user."
            )

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=["av"])
    async def avatar(self, ctx, *, user: UserFetchConverter = None):
        """Shows a user's enlarged avatar (if possible)."""
        if user is None:
            return await ctx.send(
                f"{ctx.author.mention} I can't look for nothing. Try inputting something?"
            )
        embed = discord.Embed(
            color=random.randint(0, 0xFFFFFF), timestamp=datetime.datetime.utcnow()
        )
        avatar = user.avatar_url
        embed.set_author(name=str(user), url=avatar)
        embed.set_image(url=avatar)
        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command(aliases=['emote', 'emoticon'])
    async def emoji(
        self, ctx, emoji: typing.Union[discord.Emoji, discord.PartialEmoji, str] = None
    ):
        """Shows you an enlarged version of an emoji"""
        if emoji is None:
            return await ctx.send("Try typing an actual emoji? smh.")
        if isinstance(emoji, str):
            codepoints = [ord(c) for c in emoji]
            if (
                codepoints[1:2] == [0xFE0F]
                and len(codepoints) <= 4
                and codepoints != [0x1F3F3, 0xFE0F, 0x200D, 0x1F308]
            ):
                codepoints = [codepoints[0], *codepoints[2:]]
            filename = "-".join(hex(c)[2:] for c in codepoints)
            emoji_url = f"https://github.com/twitter/twemoji/raw/master/assets/72x72/{filename}.png"
            name = emoji
            is_unicode = True
        else:
            emoji_url = str(emoji.url)
            name = emoji.name
        if not await is_valid_image(emoji_url):
            return await ctx.send(f"{ctx.author.mention} I couldn't find that emoji!")
        if isinstance(emoji, str):
            embed = discord.Embed(
                description=f"name: {name}",
                colour=random.randint(0, 0xFFFFFF),
                timestamp=datetime.datetime.utcnow(),
            )
        else:
            embed = discord.Embed(
                description=f"name `{name}` id: `{emoji.id}`",
                colour=random.randint(0, 0xFFFFFF),
                timestamp=datetime.datetime.utcnow(),
            )
        embed.set_footer(text=ctx.author.name, icon_url=ctx.author.avatar_url)
        embed.set_image(url=emoji_url)
        await ctx.send(embed=embed)

    @commands.guild_only()
    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.command(aliases=["maths", "calculator"])
    async def math(self, ctx, *, input: str = None):
        """Works just like a calculator"""
        if input is None:
            return await ctx.send(f"{ctx.author.mention} This command requires input")
        expr = input
        async with aiohttp.request(
            "POST", "http://api.mathjs.org/v4/", json={"expr": expr}
        ) as resp:
            data = await resp.json()
            await ctx.send(
                embed=discord.Embed(color=random.randint(0, 0xFFFFFF))
                .add_field(name="Input", value=f"`{expr}`")
                .add_field(
                    name="Output",
                    value=f"`{data['result']}`"
                    if data["error"] is None
                    else f"`{data['error']}`",
                )
            )

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()  # convert command
    async def convert(
        self,
        ctx,
        amount: typing.Union[float, str] = None,
        from_: str = None,
        to_: str = None,
    ):
        """Converts one currency to another.
        Use 'convert list' to get a list of available currencies."""

        currencies = """
AUD    Australian dollar
BGN    Bulgarian lev
BRL    Brazilian real
CAD    Canadian dollar
CHF    Swiss franc
CNY    Chinese yuan renminbi
CZK    Czech koruna
DKK    Danish krone
GBP    Pound sterling
HKD    Hong Kong dollar
HRK    Croatian kuna
HUF    Hungarian forint
IDR    Indonesian rupiah
ILS    Israeli shekel
INR    Indian rupee
ISK    Icelandic krona
JPY    Japanese yen
KRW    South Korean won
MXN    Mexican peso
MYR    Malaysian ringgit
NOK    Norwegian krone
NZD    New Zealand dollar
PHP    Philippine peso
PLN    Polish zloty
RON    Romanian leu
RUB    Russian rouble
SEK    Swedish krona
SGD    Singapore dollar
THB    Thai baht
TRY    Turkish lira
USD    US dollar
ZAR    South African rand"""

        if amount == "list":
            list = discord.Embed(
                title="Available currencies.",
                description=f"```{currencies}```",
                color=random.randint(0, 0xFFFFFF),
            )
            return await ctx.send(embed=list)
        elif amount is None or from_ is None or to_ is None:
            return await ctx.send(
                f"{ctx.author.mention} command usage: `c!convert <amount> <currency> <currency>`"
            )
        elif amount < 0:
            return await ctx.send("You can't use negative numbers! smh")
        from_ = from_.upper()
        to_ = to_.upper()
        try:
            async with aiohttp.request(
                "get", f"https://api.exchangeratesapi.io/latest?base={from_}"
            ) as resp:
                data = await resp.json()
            await ctx.send(
                f"{amount:.2f} {from_} = {(data['rates'][to_] * amount):.2f} {to_}"
            )
        except:
            await ctx.send(
                f"{ctx.author.mention} We don't seem to know one or more of these currency's!"
            )

    @convert.error
    async def on_error(self, ctx, error):
        user = (await self.bot.application_info()).owner
        await user.send(f"```py\n{error}```")

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.command()
    async def ping(self, ctx):
        """displays my ping and acknowledgment."""
        start = time.monotonic()
        msg = await ctx.send("Checking Ping...")
        millis = (time.monotonic() - start) * 1000

        heartbeat = ctx.bot.latency * 1000

        embed = discord.Embed(
            title="Pong! :ping_pong:",
            timestamp=(datetime.datetime.utcnow()),
            color=random.randint(0, 0xFFFFFF),
        )
        embed.add_field(name="heartbeat:", value=f"{heartbeat:,.2f}ms", inline=True)
        embed.add_field(name="Ack:", value=f"{millis:,.2f}ms", inline=True)
        await msg.edit(content="", embed=embed)

    @commands.Cog.listener()
    async def on_connect(self):
        self.bot.started_at = time.perf_counter()

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.command()
    async def uptime(self, ctx):
        """Displays how long i've been online"""
        uptime = int(time.perf_counter() - self.bot.started_at)

        mins, secs = divmod(uptime, 60)
        hours, mins = divmod(mins, 60)
        days, hours = divmod(hours, 24)
        secs = round(secs)

        day_str = f"{days} days, " if days > 0 else ""
        hour_str = f"{hours} hours, " if hours > 0 else ""
        min_str = f"{mins} mins, " if mins > 0 else ""

        embed = discord.Embed(
            title="I've been online for:",
            description=f"{day_str}{hour_str}{min_str}{secs} secs.",
            color=random.randint(0, 0xFFFFFF),
        )

        await ctx.send(embed=embed)

    @commands.command(hidden=True)
    async def status(self, ctx):
        """Displays discord's status."""
        async with aiohttp.request(
            "get", "https://status.discordapp.com/api/v2/status.json"
        ) as resp:
            data = await resp.json()

            url = data["page"]["url"]
            indic = data["status"]["indicator"]
            desc = data["status"]["description"]

            embed = discord.Embed(
                title="Current discord status",
                url=f"{url}",
                color=random.randint(0, 0xFFFFFF),
            )
            embed.add_field(name="Status", value=f"```\n{desc}\n```")
            if desc != "All Systems Operational":
                embed.add_field(name="Severity", value=f"{indic}")

            await ctx.send(embed=embed)

    # Controls how the date parser "thinks".
    parser_settings = dict(
        # Discord uses UTC.
        TIMEZONE="utc",
        PREFER_DATES_FROM='future',
        # Force timezones onto stuff.
        RETURN_AS_TIMEZONE_AWARE=True,
        # We are not american, we know how to use dates properly.
        # DD / MM / YYYY the way god intended.
        DATE_ORDER='DMY',
    )

    def parse_reminder(self, reminder) -> typing.Tuple[str, datetime.datetime]:
        results = dateparser.search.search_dates(reminder, settings=self.parser_settings)

        if len(results) == 0:
            raise RuntimeError("Could not parse a date from the input")

        return results[0]

    @staticmethod
    def create_time_string(num_seconds):
        m, s = divmod(num_seconds, 60)
        h, m = divmod(m, 60)
        d, h = divmod(h, 24)
        w, d = divmod(d, 7)
        y, w = divmod(w, 52)
        time_text = []
        if y:
            time_text.append(f"**{round(y)}** year{'s'*(y>1)}")
        if w:
            time_text.append(f"**{round(w)}** week{'s'*(w>1)}")
        if d:
            time_text.append(f"**{round(d)}** day{'s'*(d>1)}")
        if h:
            time_text.append(f"**{round(h)}** hour{'s'*(h>1)}")
        if m:
            time_text.append(f"**{round(m)}** minute{'s'*(m>1)}")
        time_text.append(f"**{round(s)}** second{'s'*(s>1)}")
        return ", ".join(time_text)

    @tasks.loop(seconds=1)
    async def poll_reminders(self):
        now = datetime.datetime.now(tz=datetime.timezone.utc)
        async with self.bot.db.acquire() as conn:
            completed_reminders = await conn.fetch(
                "SELECT reminder_id, start_time, message, user_id, channel_id FROM bot.reminders WHERE end_time <= $1",
                now,
            )

            for reminder in completed_reminders:
                seconds_ago = (now - reminder["start_time"]).seconds
                time_text = self.create_time_string(seconds_ago)
                channel = self.bot.get_channel(reminder["channel_id"])
                await channel.send(
                    content=f"<@{reminder['user_id']}> {time_text} ago: {reminder['message']}",
                    allowed_mentions=discord.AllowedMentions(
                        everyone=False,
                        users=True,
                        roles=False)
                )
                await conn.execute(
                    "DELETE FROM bot.reminders WHERE reminder_id = $1",
                    reminder["reminder_id"],
                )

    @commands.Cog.listener()
    async def on_ready(self):
        await self.poll_reminders.start()

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.group(invoke_without_command=True, aliases=['reminders', 'remind', 'remindme'])
    async def reminder(self, ctx):
        await ctx.send(
            "Available subcommands:\n`add` - creates a reminder\n`list` - list all of your reminders\n`cancel <id>` - cancel a reminder"
        )

    @commands.cooldown(rate=1, per=20, type=commands.BucketType.user)
    @commands.guild_only()
    @reminder.command(aliases=['create'])
    async def add(self, ctx, *, _reminder: str = None):
        """Create a reminder"""
        if _reminder is None:
            return await ctx.send(
                f"{ctx.author.mention} I can't remind you about nothing! this command requires input.")
        try:
            time = self.parse_reminder(_reminder)
            now = datetime.datetime.now(tz=datetime.timezone.utc)
            if now > time[1]:
                ctx.command.reset_cooldown(ctx)
                return await ctx.send(f"{ctx.author.mention} Time can't be in the past!")
            reminder = _reminder.replace(time[0], '')
            if reminder is None or reminder == "":
                return await ctx.send(f"{ctx.author.mention} I can't remind you about nothing!")
            async with ctx.bot.db.acquire() as conn:
                await conn.execute(
                    "INSERT INTO bot.reminders (start_time, end_time, message, user_id, channel_id) "
                    "VALUES (NOW(), $1, $2, $3, $4);",
                    time[1], reminder, ctx.author.id, ctx.channel.id)

                embed = discord.Embed(
                    description=f"Ok! {time[0]} i'll remind you: {reminder}",
                    timestamp=datetime.datetime.utcnow(),
                    color=random.randint(0, 0xFFFFFF)
                )
            await ctx.send(embed=embed)
        except:
            ctx.command.reset_cooldown(ctx)
            return await ctx.send(f"{ctx.author.mention} Make sure to include a time and what to remind you about!")

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @reminder.command(name="list")
    async def reminder_list(self, ctx):
        """Returns all active reminders."""
        now = datetime.datetime.now(tz=datetime.timezone.utc)
        async with ctx.bot.db.acquire() as conn:
            rows = await conn.fetch(
                "SELECT reminder_id, message, end_time FROM bot.reminders WHERE user_id = $1;",
                ctx.author.id
            )
        reminders = []
        for record in rows:
            time_left = (record['end_time'] - now).total_seconds()
            time_str = self.create_time_string(time_left)
            reminders.append(f"**{record['reminder_id']}**: {record['message']} | in {time_str}")
        message = "\n".join(reminders)
        if message is None or message == "":
            return await ctx.send(f"{ctx.author.mention} It seems you don't have any reminders.")

        embed = discord.Embed(
            title="Reminders",
            description=message,
            timestamp=datetime.datetime.utcnow(),
            color=random.randint(0, 0xFFFFFF)
        )
        await ctx.send(embed=embed)

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @reminder.command(name='cancel')
    async def reminder_cancel(self, ctx, reminder_id: int = None):
        """Deletes a reminder using the reminder ID (use the reminders command to find the reminder ID)"""
        if reminder_id is None:
            return await ctx.send(f"{ctx.author.mention} Reminder ID can't be none.")
        async with ctx.bot.db.acquire() as conn:
            try:
                await conn.execute(
                    "DELETE FROM bot.reminders WHERE reminder_id = $1 AND user_id = $2",
                    reminder_id,
                    ctx.author.id,
                )
                return await ctx.send(f"{ctx.author.mention} Reminder cancelled successfully")
            except:
                return await ctx.send(f"{ctx.author.mention} I can't seem to find that reminder.")

    @commands.is_owner()
    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @commands.group(invoke_without_command=True)
    async def starboard(self, ctx):
        await ctx.send(
            "Available subcommands:\n`set` - set the starboard channel.\n`remove` - removes current starboard."
        )

    @commands.is_owner()
    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @starboard.command()
    async def set(self, ctx):
        """Sets the starboard channel"""

    @commands.is_owner()
    @commands.cooldown(rate=1, per=5, type=commands.BucketType.user)
    @commands.guild_only()
    @starboard.command()
    async def remove(self, ctx):
        """Removes the current starboard channel"""


def setup(bot):
    bot.add_cog(Utility(bot))
