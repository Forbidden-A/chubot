import discord
from discord.ext import commands
import datetime
import random


class CustomHelpCommand(commands.HelpCommand):
    async def send_bot_help(self, mapping):
        emb = discord.Embed(
            title="Command list",
            description=f"Use `{self.clean_prefix}help [command]` for more info on a command.",
            timestamp=datetime.datetime.utcnow(),
            colour=random.randint(0, 0xFFFFFF),
        )
        emb.add_field(
            name=":shield: Moderation",
            value=f"`{self.clean_prefix}help moderation`",
            inline=True,
        )
        emb.add_field(
            name=":toolbox: Utility",
            value=f"`{self.clean_prefix}help utility`",
            inline=True,
        )
        emb.add_field(
            name=":smile: Fun", value=f"`{self.clean_prefix}help fun`", inline=True
        )
        emb.add_field(
            name=":camera: Image ",
            value=f"`{self.clean_prefix}help image`",
            inline=True,
        )
        emb.add_field(
            name=":iphone: Emotes ",
            value=f"`{self.clean_prefix}help emotes`",
            inline=True,
        )
        emb.add_field(
            name=":books: Misc ", value=f"`{self.clean_prefix}help misc`", inline=True
        )
        emb.set_footer()
        await self.context.send(embed=emb)

    async def send_cog_help(self, cog):
        cog_commands = cog.get_commands()
        e = discord.Embed(
            title=f"{cog.qualified_name} commands",
            description=", ".join([f"`{c.name}`" for c in cog_commands]),
            timestamp=datetime.datetime.utcnow(),
            color=random.randint(0, 0xFFFFFF),
        )
        e.set_footer(text="use c!help (command) for more info on a command")
        await self.get_destination().send(embed=e)

    async def send_command_help(self, command):
        emb = discord.Embed(
            title=f"Detailed help for __{command}__",
            description=f"```{self.get_command_signature(command)}```",
            colour=random.randint(0, 0xFFFFFF),
        )
        if not command.help:
            pass
        else:
            emb.add_field(
                name="Details",
                value=f"```{command.help}```" or "No details available.",
                inline=False,
            )
        if not command.aliases:
            pass
        else:
            emb.add_field(
                name="Aliases",
                value=f"```{', '.join(command.aliases)}```",
                inline=False,
            )
        await self.context.send(embed=emb)

    async def send_group_help(self, group):
        group_commands = group.all_commands
        e = discord.Embed(
            title=f"Group help for {group}",
            description=", ".join([f"`{c}`" for c in group_commands]),
            timestamp=datetime.datetime.utcnow(),
            color=random.randint(0, 0xFFFFFF)
        )
        await self.context.send(embed=e)

    async def command_not_found(self, string):
        return f"{self.context.author.mention} command `{string}` doesnt exist."

    def get_command_signature(self, command):
        return "{0.clean_prefix}{1.qualified_name} {1.signature}".format(self, command)


class Help(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self._original_help_command = bot.help_command
        bot.help_command = CustomHelpCommand(
            command_attrs={"aliases": ["h", "?"], "hidden": True}
        )
        bot.help_command.cog = self

    def cog_unload(self):
        self.bot.help_command = self._original_help_command


def setup(bot):
    bot.add_cog(Help(bot))
