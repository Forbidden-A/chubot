import discord
from discord.ext import commands

import datetime
import logging
import os
import random
import aiohttp
import traceback

from bot.utils import blacklist

logging.basicConfig(level=os.getenv("LOGGER_VERBOSITY", "INFO").upper())
logger = logging.getLogger("Chubonyo_Bot")


class Listeners(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()  # Global error handler
    async def on_command_error(self, ctx, error):
        error = error.__cause__ or error
        if isinstance(error, blacklist.Blacklisted):
            return
        if isinstance(error, commands.CommandOnCooldown):
            desc = "Please wait `{seconds}` seconds before using the `{command}` command again.".format(
                command=ctx.command.name, seconds=round(error.retry_after, 2)
            )
            embed = discord.Embed(
                description=desc,
                timestamp=datetime.datetime.utcnow(),
                color=random.randint(0, 0xFFFFFF),
            )
            await ctx.send(embed=embed)
            return
        if isinstance(error, commands.NSFWChannelRequired):
            return await ctx.send(
                f"{ctx.author.mention} This command can only be used in a NSFW channel."
            )
        if isinstance(error, ValueError):
            return await ctx.send(f'{ctx.author.mention} Make sure your input is correct!')
        if isinstance(error, commands.CommandNotFound):
            return
        else:
            user = (await self.bot.application_info()).owner

            err = "".join(traceback.format_exception(type(error), error, error.__traceback__))
            async with aiohttp.request("POST", "https://hastebin.com/documents", data=err) as resp:
                resp.raise_for_status()
                data = await resp.json()

            return await user.send(f"```py\n{error}```\nhttps://hastebin.com/{data['key']}")

    @commands.Cog.listener()
    async def on_ready(self):
        logger.info("Logged in as: %s", self.bot.user.name)
        logger.info("With ID: %s", self.bot.user.id)
        members = sum(1 for _ in self.bot.get_all_members())
        guilds = len(self.bot.guilds)
        await self.bot.change_presence(
            activity=discord.Game(
                f"Use c!help | Connected to {members} members in {guilds} guilds"
            )
        )

    @commands.Cog.listener()  # auto reactions to messages
    async def on_message(self, message):
        if message.author.bot:
            return

        if message.content == "Chubonyo" or message.content == "Chuboñyo":
            x = random.randint(0, 1000)
            if x == 69:
                await message.channel.send("<3 (´∩｡• ᵕ •｡∩`) ♡")

    @commands.Cog.listener()
    async def on_guild_join(self, guild):
        async with self.bot.db.acquire() as conn:
            logger.info("Adding default prefix to db for new guild: %s", guild.name)
            await conn.execute(
                "INSERT INTO bot.prefixes(guild_id, prefix) VALUES($1, $2) ON CONFLICT (guild_id) DO NOTHING;",
                guild.id,
                "c!",
            )


def setup(bot):
    bot.add_cog(Listeners(bot))
