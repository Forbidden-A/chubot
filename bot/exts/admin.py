import inspect
import random
import sys
import traceback
import discord
import datetime
import os
import json

import libneko
from discord.ext import commands
from discord.ext import tasks

from bot.utils import blacklist, premium

import asyncio


class Admin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.is_owner()
    @commands.guild_only()
    @commands.command()
    async def forceleave(self, ctx):
        """Makes me force leave the server"""
        await ctx.send("As you wish master.")
        return await ctx.guild.leave()

    @commands.is_owner()
    @commands.group(invoke_without_command=True, hidden=True)
    async def blacklist(self, ctx):
        pass

    @commands.is_owner()
    @blacklist.command()
    async def add(self, ctx, entity_type: str, entity_id: int):
        if entity_id == 332857142522413056:
            return await ctx.send("No master you can't :c")
        entity_type = entity_type.lower()
        if entity_type not in ["user", "guild"]:
            return await ctx.send("Entity type must be one of `user`, `guild`")
        await blacklist.add_entity_to_blacklist(ctx, entity_id=entity_id, entity_type=entity_type)
        return await ctx.send("it is done master")

    @commands.is_owner()
    @blacklist.command()
    async def remove(self, ctx, entity_id: int):
        await blacklist.remove_entity_from_blacklist(ctx, entity_id=entity_id)
        return await ctx.send("it is done master")

    @commands.is_owner()
    @commands.group(invoke_without_command=True, hidden=True, aliases=['setprem', 'setpremium'])
    async def prem(self, ctx):
        pass

    @commands.is_owner()
    @prem.command()
    async def set(self, ctx, entity_type, entity_id: int):
        entity_type = entity_type.lower()
        if entity_type not in ["user", "guild"]:
            return await ctx.send("Entity type must be one of `user`, `guild`")
        await premium.add_entity_to_premium(ctx, entity_id, entity_type)
        return await ctx.send("it is done master")

    @commands.is_owner()
    @prem.command()
    async def delete(self, ctx, entity_id: int):
        await premium.remove_entity_from_premium(ctx, entity_id)
        return await ctx.send("it is done master")

    @commands.is_owner()
    @commands.command(hidden=True)
    async def shutdown(self, ctx):
        print(f"I was shutdown by User: {ctx.author.display_name} ID: {ctx.author.id}")
        embed = discord.Embed(
            title=f"Shutting down...", color=random.randint(0, 0xFFFFFF)
        )
        msg = await ctx.send(embed=embed)  # only runs if owner uses command
        await msg.edit(
            content="",
            embed=discord.Embed(
                title=f"shutdown successful", color=random.randint(0, 0xFFFFFF)
            ),
        )
        await self.bot.logout()

    @commands.is_owner()
    @commands.command(hidden=True)
    async def reboot(self, ctx):
        extensions = ctx.bot.extensions.copy()
        for name in extensions.keys():
            ctx.bot.reload_extension(name)

        await ctx.send("Bot restarted")

    @commands.is_owner()
    @commands.command(hidden=True)
    async def load(self, ctx, *, extension):
        try:
            self.bot.load_extension(f"exts.{extension}")

        except Exception as ex:
            await self._send_error(ctx, ex)

        else:
            embed = discord.Embed(
                title=f"Successfully loaded: __{extension}__ :ok_hand_tone1:",
                color=random.randint(0, 0xFFFFFF),
                timestamp=datetime.datetime.utcnow(),
            )
            await ctx.send(embed=embed)

    @commands.is_owner()
    @commands.command(hidden=True)
    async def unload(self, ctx, *, extension):
        try:
            self.bot.unload_extension(f"exts.{extension}")

        except Exception as ex:
            await self._send_error(ctx, ex)

        else:
            embed = discord.Embed(
                title=f"Successfully unloaded: __{extension}__ :ok_hand_tone1:",
                color=random.randint(0, 0xFFFFFF),
                timestamp=datetime.datetime.utcnow(),
            )
            await ctx.send(embed=embed)

    @commands.is_owner()
    @commands.command(hidden=True)
    async def reload(self, ctx, *, extension):
        try:
            self.bot.reload_extension(f"exts.{extension}")

        except Exception as ex:
            await self._send_error(ctx, ex)

        else:
            embed = discord.Embed(
                title=f"Successfully reloaded: __{extension}__ :ok_hand_tone1:",
                color=random.randint(0, 0xFFFFFF),
                timestamp=datetime.datetime.utcnow(),
            )
            await ctx.send(embed=embed)

    @commands.is_owner()
    @commands.command(aliases=["code"], hidden=True)
    async def source(self, ctx, *, command):
        cmd = self.bot.get_command(command)
        if not cmd:
            return await ctx.send("No such command.")
        cmd = inspect.getsource(cmd.callback)
        cmd = "\n".join([line[4:] for line in cmd.splitlines() if line.startswith('    ')])

        @libneko.embed_generator(max_chars=1525)
        def make_embed(paginator, page, index):
            return discord.Embed(
                title=f"{command}'s source",
                colour=random.randint(0, 0xFFFFFF),
                description=f"```py\n# Python {sys.version}\n{page}```",
            )

        pag = libneko.EmbedNavigatorFactory(factory=make_embed, max_lines=30)
        for line in cmd.splitlines():
            pag.add_line(line.replace("`", "´"))
        pag.start(ctx=ctx, timeout=60.0)

    async def _send_error(self, ctx, ex):
        ex_msg = "".join(traceback.format_exception(type(ex), ex, ex.__traceback__))
        paginator = commands.Paginator()
        for line in ex_msg.splitlines():
            paginator.add_line(line)

        for page in paginator.pages:
            await ctx.send(page)

    @tasks.loop(hours=1)
    async def snek_loop(self):
        channel = self.bot.get_channel(625063966564941861)
        await channel.send(
            "Join the loggers revolution make this your profile picture! https://cdn.discordapp.com/attachments/265828729970753537/735133805806878780/653988922673725489.png"
        )

    @commands.is_owner()
    @commands.command(hidden=True)
    async def snek(self, ctx):
        await self.snek_loop.start()


def setup(bot):
    bot.add_cog(Admin(bot))
