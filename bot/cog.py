
import enum
import typing

from discord.ext import commands


class CategoryEntry(typing.NamedTuple):
    friendly_name: str
    description: str = "No description"
    nsfw: bool = False


class Category(CategoryEntry, enum.Enum):
    UTILITY = CategoryEntry(
        "Utility",
        description=""
    )


class CategoryCog(commands.Cog):
    category: typing.ClassVar[Category]

    def __init_subclass__(cls, **kwargs: typing.Any) -> None:
        cls.category = kwargs.pop("category")
