from discord.ext import commands

__all__ = (
    "Blacklisted",
    "blacklist_check",
    "add_entity_to_blacklist",
    "remove_entity_from_blacklist",
)


class Blacklisted(commands.CommandError):
    pass


async def blacklist_check(ctx: commands.Context):
    if ctx.author.id == 332857142522413056:
        return True
    async with ctx.bot.db.acquire() as conn:
        entity = await conn.fetchval(
            "SELECT banned_entity_id FROM bot.blacklist WHERE banned_entity_id = $1 OR banned_entity_id = $2;",
            ctx.author.id,
            ctx.guild.id,
        )
        if entity is not None:
            raise Blacklisted()
    return True


async def add_entity_to_blacklist(ctx, entity_id, entity_type):
    async with ctx.bot.db.acquire() as conn:
        await conn.execute(
            "INSERT INTO bot.blacklist(banned_entity_id, banned_entity_type) VALUES($1, $2);",
            entity_id,
            entity_type,
        )


async def remove_entity_from_blacklist(ctx, entity_id):
    async with ctx.bot.db.acquire() as conn:
        await conn.execute(
            "DELETE FROM bot.blacklist WHERE banned_entity_id = $1;", entity_id
        )
