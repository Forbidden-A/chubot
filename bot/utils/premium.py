from discord.ext import commands

__all__ = (
    "user_has_premium",
    "guild_has_premium",
    "add_entity_to_premium",
    "remove_entity_from_premium",
)


async def _check_has_premium(bot, entity_id, entity_type):
    returned_id = await bot.db.fetchval(
        "SELECT entity_id FROM bot.premium WHERE entity_id = $1 AND entity_type = $2;",
        entity_id,
        entity_type,
    )
    return returned_id is not None and returned_id == entity_id


def user_has_premium():
    async def decorate(ctx):
        return await _check_has_premium(ctx.bot, ctx.author.id, "user")

    return commands.check(decorate)


def guild_has_premium():
    async def decorate(ctx):
        return ctx.guild is not None and await _check_has_premium(
            ctx.bot, ctx.guild.id, "guild"
        )

    return commands.check(decorate)


async def add_entity_to_premium(ctx, entity_id, entity_type):
    async with ctx.bot.db.acquire() as conn:
        await conn.execute(
            "INSERT INTO bot.premium(entity_id, entity_type) VALUES($1, $2);",
            entity_id,
            entity_type,
        )


async def remove_entity_from_premium(ctx, entity_id):
    async with ctx.bot.db.acquire() as conn:
        await conn.execute(
            "DELETE FROM bot.premium WHERE entity_id = $1;", entity_id
        )
