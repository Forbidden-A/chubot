# Chubonyo App
from discord.ext import commands

import logging
import os
import json

from bot.mrbotto import MrB


def get_logger_format():
    # Copied from hikari.impl.bot#BotAppImpl.__get_logging_format v2.0.0.dev17 with permission from nekoka
    import sys

    plat = sys.platform
    supports_color = False

    # isatty is not always implemented, https://code.djangoproject.com/ticket/6223
    is_a_tty = hasattr(sys.stdout, "isatty") and sys.stdout.isatty()

    if plat != "Pocket PC":
        if plat == "win32":
            supports_color |= os.getenv("TERM_PROGRAM", None) == "mintty"
            supports_color |= "ANSICON" in os.environ
            supports_color |= is_a_tty
        else:
            supports_color = is_a_tty

        supports_color |= bool(os.getenv("PYCHARM_HOSTED", ""))

    if supports_color:
        blue = "\033[1;35m"
        gray = "\033[1;37m"
        green = "\033[1;32m"
        red = "\033[1;31m"
        yellow = "\033[1;33m"
        default = "\033[0m"
    else:
        blue = gray = green = red = yellow = default = ""

    return (
        f"{red}%(levelname)-1.1s {yellow}%(name)-25.25s {green}#%(lineno)-4d {blue}%(asctime)23.23s"
        f"{default} :: {gray}%(message)s{default}"
    )


# Default prefix = 'c!'
logging.basicConfig(
    level=os.getenv("LOGGER_VERBOSITY", "INFO").upper(), format=get_logger_format()
)
LOGGER = logging.getLogger("chubonyo_bot")

# all the command extensions :)
EXTENSIONS = [
    "bot.exts.admin",
    "bot.exts.emotes",
    "bot.exts.exec",
    "bot.exts.fun",
    "bot.exts.listeners",
    "bot.exts.image",
    "bot.exts.misc",
    "bot.exts.moderation",
    "bot.exts.help",
    "bot.exts.utility",
]


def main():
    with open("Config.json") as fp:
        config = json.load(fp)

    bot = MrB(config)

    for ext in EXTENSIONS:
        try:
            bot.load_extension(ext)
        except commands.errors.ExtensionNotFound:
            LOGGER.warning("Extension `%s` was not found.", ext)
        except commands.errors.ExtensionFailed as error:
            LOGGER.error(
                "Failed to load extension `%s`, reason: %s",
                ext,
                error.__cause__,
                exc_info=error,
            )
        except commands.errors.NoEntryPointError:
            LOGGER.warning("Extension `%s` has no setup function!", ext)

    bot.run(bot.config["token"])


if __name__ == "__main__":
    main()
