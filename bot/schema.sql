CREATE SCHEMA IF NOT EXISTS bot;

DO $$
BEGIN
    CREATE TYPE bot.premium_type AS ENUM ('guild', 'user');
EXCEPTION
    WHEN duplicate_object THEN null;
END
$$ LANGUAGE plpgsql;

CREATE TABLE IF NOT EXISTS bot.premium (
    entity_id    BIGINT            NOT NULL,
    entity_type  bot.premium_type  NOT NULL,
    PRIMARY KEY(entity_id, entity_type)
);

DO $$
BEGIN
    CREATE TYPE bot.blacklist_type AS ENUM ('guild', 'user');
EXCEPTION
    WHEN duplicate_object THEN null;
END
$$ LANGUAGE plpgsql;

CREATE TABLE IF NOT EXISTS bot.blacklist (
    banned_entity_id      BIGINT                NOT NULL,
    banned_entity_type    bot.blacklist_type    NOT NULL,
    PRIMARY KEY(banned_entity_id, banned_entity_type)
);

CREATE TABLE IF NOT EXISTS bot.starboard (
    star_guild_id      BIGINT    PRIMARY KEY,
    star_channel_id    BIGINT    NOT NULL
);

CREATE TABLE IF NOT EXISTS bot.star_messages (
    starred_message    BIGINT    PRIMARY KEY,
    message_sent       BIGINT    NOT NULL
);

CREATE TABLE IF NOT EXISTS bot.reminders (
    reminder_id    SERIAL         PRIMARY KEY,
    start_time     TIMESTAMPTZ    NOT NULL,
    end_time       TIMESTAMPTZ    NOT NULL,
    message        TEXT           NOT NULL,
    user_id        BIGINT         NOT NULL,
    channel_id     BIGINT         NOT NULL
);

CREATE TABLE IF NOT EXISTS bot.tags (
    tag_id         SERIAL    PRIMARY KEY,
    tag_user_id    BIGINT    NOT NULL,
    title          TEXT      NOT NULL,
    content        TEXT      NOT NULL
);

CREATE TABLE IF NOT EXISTS bot.prefixes (
     guild_id    BIGINT         PRIMARY KEY,
     prefix      VARCHAR(5),

     CONSTRAINT valid_prefix_chk  CHECK  (TRIM(prefix) <> '')
);

CREATE TABLE IF NOT EXISTS bot.economy (
    eco_user_id        BIGINT         PRIMARY KEY,
    multiplier         INTEGER        NOT NULL DEFAULT 1,
    wallet             INTEGER        NOT NULL DEFAULT 0,
    bank               INTEGER        NOT NULL DEFAULT 0,
    exp                INTEGER        NOT NULL DEFAULT 0,
    daily_cooldown     TIMESTAMPTZ    DEFAULT NULL,
    weekly_cooldown    TIMESTAMPTZ    DEFAULT NULL,
    job_id             INTEGER        DEFAULT NULL,
    commands_run       INTEGER        NOT NULL DEFAULT 0
);

CREATE TABLE IF NOT EXISTS bot.rpg_system (
    rpg_user_id       BIGINT         PRIMARY KEY,
    level             INTEGER        NOT NULL DEFAULT 1,
    unspent_points    INTEGER        NOT NULL DEFAULT 0,
    attack_points     INTEGER        NOT NULL DEFAULT 0,
    spell_points      INTEGER        NOT NULL DEFAULT 0,
    defence_points    INTEGER        NOT NULL DEFAULT 0,
    health_points     INTEGER        NOT NULL DEFAULT 0,
    enemies_killed    INTEGER        NOT NULL DEFAULT 0,
    bosses_killed     INTEGER        NOT NULL DEFAULT 0,
    boss_cooldown     TIMESTAMPTZ    DEFAULT NULL
);