import datetime

import pytest

from bot.exts import reminders


@pytest.fixture
def cog():
    return reminders.Reminders()


def test_parse_date(cog):
    assert cog.parse_reminder("26/07/2020 12:00:00") == datetime.datetime(2020, 7, 26, 12, 0, 0, tzinfo=datetime.timezone.utc)
